import { Component, OnInit, DoCheck } from '@angular/core';
@Component({
    // tslint:disable-next-line:component-selector
    selector: 'main',
    templateUrl: './main.component.html'
})
export class MainComponent implements OnInit {
    public title: string;
    constructor() {
        this.title = 'Mensajeria';
    }

    ngOnInit() {
        console.log('Mensajeria cargada!');
    }
}
