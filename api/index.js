'use strict'

var mongoose = require('mongoose');
var app = require('./app');
var port = 3800;
// promesas - conexion a mongodb
mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost:27017/red_social', { useNewUrlParser: true })
        .then(()=>{
            console.log('La conexion a la base de datos "red_social" se realizo con exito');
            // Crear servidor
            app.listen(port, '0.0.0.0',  () => {
                console.log('Servidor corriendo en localhost:3800');
            });
        })
        .catch(err => console.log(err));